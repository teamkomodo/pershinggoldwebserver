/**
 * Config Files
 */

module.exports = {
  /*
   *
   */
  //defaultController: 'MAIN',

  /*
   *
   */
  controllers: [

	 { host: '192.168.101.2', /*0.49 for test setup*/
	  slot: 1, /* slot 2 for test setup */
      connection_name: 'PERSHING',
      port: 102,
      type: 'nodes7',
      tagfile: './pershing_gold_tags.txt' }
  
  ],

  /*
   * Tagsets
   * TagSet names must be predefined here only then can a tag be associated.
   * Otherwise during tag registering process you will receive a TagSet Does Not
   * Exist Error.
   */
  tagsets: [
    'status'
  ],

  /*
   * Warnings
   * Similar to TagSet but only will only be used for warning tags
   */
  warnings: [    
    'master_warnings'
  ],

  logs:  {
	'main':
		{ interval: 1 },
	'secondary':
		{ interval: 5 }
	},
  
  /*
   * Tags
   * All tags that will be used in the application. Only tags registered here
   * and associated to a valid Tagset or Warningset will be available to the 
   * client views.
   * 
   * Options:
   * TODO: create complete list of options
   */
  tags : {  	
    'PERSHING/WEIGHSCALE_DATA.JAW_PLANT.REGISTERS.AVERAGE_RATE':{
      tagsets:['status'],
	  logs:['main'],
	  header:'Jaw Plant Current Rate'
    },
	'PERSHING/WEIGHSCALE_DATA.JAW_PLANT.REGISTERS.TOTAL':{
      tagsets:['status'],
	  logs:['main'],
	  header:'Jaw Plant Total'
    },
	'PERSHING/WEIGHSCALE_DATA.OVERLAND_PLANT.REGISTERS.AVERAGE_RATE':{
	  tagsets:['status'],
	  logs:['main'],
	  header:'Overland Plant Current Rate'
	},
    'PERSHING/WEIGHSCALE_DATA.OVERLAND_PLANT.REGISTERS.TOTAL':{
	  tagsets:['status'],
	  logs:['main'],
	  header:'Overland Plant Total'
	},
    'PERSHING/WEIGHSCALE_DATA.AGGLOMERATION.REGISTERS.AVERAGE_RATE':{
	  tagsets:['status'],
	  logs:['main'],
	  header:'Agglomeration Current Rate'
	},
    'PERSHING/WEIGHSCALE_DATA.AGGLOMERATION.REGISTERS.TOTAL':{
	  tagsets:['status'],
	  logs:['main'],
	  header:'Agglomeration Total'
	}
  }
}

/*
,
	'PUMP/ENG_TRANS_DB.ENGINE.SPEED':{
      tagsets:['status'],
	  logs:['secondary'],
	  header:'Engine Speed'	
	},
	'PUMP/ENG_TRANS_DB.TRANS.GEAR_ATT':{
      tagsets:['status'],
	  logs:['secondary'],
	  header:'Trans Gear'	
	}
*/
